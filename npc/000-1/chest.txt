// Evol scripts.
// Authors:
//    4144
//    Hal9000
//    Reid
// Description:
//    A treasure chest, with... Treasures inside!
// Variable:
//    x   ShipQuests_TreasureChest
// Values:
//    0   Treasure Chest has not been opened yet.
//    1   Treasure Chest has been opened.

000-1,83,70,0	script	Chest#000-1	NPC_CHEST,{
    if (.dir != 4)
    {
        .dir = 2;
        initnpctimer;
        startnpctimer;
        misceffect 24;
        close;
    }

    .@q = getq(ShipQuests_TreasureChest);
    if (.@q == 0)
    {
        inventoryplace OldBook, 1;
        setq ShipQuests_TreasureChest, 1;
        Zeny = Zeny + 100;
        getitem OldBook, 1;
        npctalk3 l("Chest opened.");
    }

    .dir = 6;
    initnpctimer;
    startnpctimer;
    misceffect 25;
    close;

OnTimer160:
    stopnpctimer;
    if (.dir == 2) .dir = 4;
    if (.dir == 6) .dir = 0;
    end;

OnInit:
    .distance = 2;
    end;
}
